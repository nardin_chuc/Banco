-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-09-2022 a las 02:39:06
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `banco`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE `historial` (
  `ID_MovimientoServicio` int(11) NOT NULL,
  `ID_Servicio` int(11) NOT NULL,
  `NumeroTarjeta` varchar(16) NOT NULL,
  `montoPagar` float NOT NULL,
  `Fecha` varchar(10) NOT NULL,
  `Estado` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`ID_MovimientoServicio`, `ID_Servicio`, `NumeroTarjeta`, `montoPagar`, `Fecha`, `Estado`) VALUES
(1, 1, '1234567890123456', 100, '31/08/22', 'Exitoso'),
(2, 1, '1234567890123456', 50, '31/08/22', 'Exitoso'),
(3, 1, '1234567890123456', 50, '31/08/22', 'Exitoso'),
(4, 1, '1234567890123456', 300, '31/08/22', 'Rechazado'),
(5, 1, '1234567890123456', 300, '31/08/22', 'Rechazado'),
(6, 1, '1234567890123456', 300, '31/08/22', 'Rechazado'),
(7, 1, '1234567890123456', 300, '31/08/22', 'Exitoso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `ID_Servicio` int(11) NOT NULL,
  `NombreServicio` varchar(15) NOT NULL,
  `Estado` varchar(10) NOT NULL DEFAULT 'Activo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`ID_Servicio`, `NombreServicio`, `Estado`) VALUES
(1, 'LUZ', 'Activo'),
(2, 'TV', 'Activo'),
(3, 'AGUA', 'Activo'),
(4, 'INTERNET', 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarjetas`
--

CREATE TABLE `tarjetas` (
  `NombreTitular` varchar(30) NOT NULL,
  `NumeroTarjeta` varchar(16) NOT NULL,
  `Saldo` float NOT NULL DEFAULT 0,
  `Fecha` varchar(5) NOT NULL,
  `CVV` int(2) NOT NULL,
  `ID_Usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tarjetas`
--

INSERT INTO `tarjetas` (`NombreTitular`, `NumeroTarjeta`, `Saldo`, `Fecha`, `CVV`, `ID_Usuario`) VALUES
('NARDIN ALEXANDER CHUC CAAMAL', '1234567890123456', 500, '01/25', 123, 1),
('MARIA ISABEL CHUC CAAMAL', '1592634872581473', 1000, '01/25', 1234, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID_Usuario` int(11) NOT NULL,
  `NombreTitular` varchar(30) NOT NULL,
  `Usuario` varchar(20) NOT NULL,
  `Contraseña` varchar(30) NOT NULL,
  `Estado` varchar(10) NOT NULL DEFAULT 'Activo',
  `ROL` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID_Usuario`, `NombreTitular`, `Usuario`, `Contraseña`, `Estado`, `ROL`) VALUES
(1, 'NARDIN ALEXANDER CHUC CAAMAL', 'Alex', 'chuc1234', 'Activo', 'Cliente'),
(2, 'NARDIN ALEXANDER CHUC CAAMAL', 'Admin', 'admin', 'Activo', 'Administrador'),
(3, 'MARIA ISABEL CHUC CAAMAL', 'maria10', 'isa10', 'Activo', 'Cliente');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
  ADD PRIMARY KEY (`ID_MovimientoServicio`),
  ADD KEY `ID_MovimientoServicio` (`ID_MovimientoServicio`),
  ADD KEY `NumeroTarjeta` (`NumeroTarjeta`),
  ADD KEY `ID_Servicio` (`ID_Servicio`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`ID_Servicio`);

--
-- Indices de la tabla `tarjetas`
--
ALTER TABLE `tarjetas`
  ADD PRIMARY KEY (`NumeroTarjeta`),
  ADD KEY `ID_Usuario` (`ID_Usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID_Usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
  MODIFY `ID_MovimientoServicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `ID_Servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID_Usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historial`
--
ALTER TABLE `historial`
  ADD CONSTRAINT `historial_ibfk_2` FOREIGN KEY (`NumeroTarjeta`) REFERENCES `tarjetas` (`NumeroTarjeta`),
  ADD CONSTRAINT `historial_ibfk_3` FOREIGN KEY (`ID_Servicio`) REFERENCES `servicios` (`ID_Servicio`);

--
-- Filtros para la tabla `tarjetas`
--
ALTER TABLE `tarjetas`
  ADD CONSTRAINT `tarjetas_ibfk_1` FOREIGN KEY (`ID_Usuario`) REFERENCES `usuarios` (`ID_Usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
