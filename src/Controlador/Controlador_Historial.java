/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.Historial;
import Vista.Login;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CCNAR
 */
public class Controlador_Historial implements ActionListener, MouseListener {

    Historial hs;
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public Controlador_Historial(Historial hs) {
        this.hs = hs;
        this.hs.btn_regresar.addActionListener(this);
        this.hs.btn_pagar.addActionListener(this);
        this.hs.btn_regresar.addMouseListener(this);
        this.hs.btn_pagar.addMouseListener(this);
        this.hs.btn_aceptarCredenciales.addActionListener(this);
        this.hs.btn_regresar_Credenciales.addActionListener(this);
        LLenarCBX();
        cargarTabla();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == hs.btn_regresar) {
            Login lg = new Login();
            lg.setVisible(true);
            hs.setVisible(false);
        } else if (e.getSource() == hs.btn_pagar) {
            if(hs.txt_monto.getText().isEmpty()){
                JOptionPane.showMessageDialog(null, "Digite el monto a Pagar");
                hs.txt_monto.requestFocus(true);
            }else{
                hs.jf_VerificarPago.setVisible(true);
            }
        }else if (e.getSource() == hs.btn_aceptarCredenciales) {
            VerificarSaldo();
            cargarTabla();
        }else if (e.getSource() == hs.btn_regresar_Credenciales) {
            hs.jf_VerificarPago.setVisible(false);
            limpiarCredenciales();
            cargarTabla();
            
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == hs.btn_regresar) {
            hs.btn_regresar.setBackground(Color.red);
        } else if (e.getSource() == hs.btn_pagar) {
            hs.btn_pagar.setBackground(Color.red);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == hs.btn_regresar) {
            hs.btn_regresar.setBackground(Color.white);
        } else if (e.getSource() == hs.btn_pagar) {
            hs.btn_pagar.setBackground(Color.white);
        }
    }

    public void LLenarCBX() {
        try {
            hs.cbx_servicios.removeAllItems();
            String estado = "Activo";
            String sql = "SELECT NombreServicio FROM servicios WHERE Estado = ?";
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, estado);
            rs = ps.executeQuery();
            int i = 0;
            while (rs.next()) {
                String Servicio = rs.getString("NombreServicio");
                hs.cbx_servicios.addItem(Servicio);
                i++;
            }

        } catch (Exception er) {
            System.err.println("Error en cbx: " + er.toString());
        }
    }
    
    
    public void VerificarSaldo(){
        String Tarjeta = hs.lb_numTarjeta.getText();
        float monto = Float.parseFloat(hs.txt_monto.getText());
        String sql = "SELECT Saldo FROM tarjetas WHERE NumeroTarjeta  = ?";
        
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, Tarjeta);
            rs = ps.executeQuery();
            while(rs.next()){
                Float montoAux = rs.getFloat("Saldo");
                if(montoAux == 0.0 || montoAux < monto){
                    RegistrarServicio("Rechazado");
                    cargarTabla();
                    JOptionPane.showMessageDialog(null, "Saldo insuficiente, Verifique su saldo");
                    limpiarCredenciales();
                    hs.txt_monto.setText("");
                    hs.jf_VerificarPago.setVisible(false);
                }else{
                    Credenciales();
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"error Verificar Saldo: "+e.getMessage());
        }
        
    }
    

    public void RegistrarServicio(String estado) {
        String Tarjeta = hs.lb_numTarjeta.getText();
        int id_Servicio = buscarServicio();
        float monto = Float.parseFloat(hs.txt_monto.getText());
        String fecha = hs.lb_fecha.getText();
        String sql = "INSERT INTO historial (ID_Servicio, NumeroTarjeta, montoPagar, Fecha,Estado) VALUES (?,?,?,?,?)";
        String sqlPagar = "";
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id_Servicio);
            ps.setString(2, Tarjeta);
            ps.setFloat(3, monto);
            ps.setString(4, fecha);
            ps.setString(5, estado);
            ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"error Registrar: "+e.getMessage());
        }
        
    }
    
    public void ActualizarSaldo(){
        String Tarjeta = hs.lb_numTarjeta.getText();
        float monto = Float.parseFloat(hs.txt_monto.getText());
        String fecha = hs.lb_fecha.getText();
        String sql = "UPDATE tarjetas SET Saldo = Saldo  - "+monto+" WHERE NumeroTarjeta = ?";
        String sqlPagar = "";
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, Tarjeta);
            ps.executeUpdate();
            RegistrarServicio("Exitoso");
            cargarTabla();
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(null,"error Actualizar Saldo: "+e.getMessage());
        }
    }

    public int buscarServicio() {
        String Servicio = hs.cbx_servicios.getSelectedItem().toString();
        String sql = "SELECT ID_Servicio FROM servicios WHERE NombreServicio = ?";
        int id_servicio = 0;
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, Servicio);
            rs = ps.executeQuery();
            while(rs.next()){
                id_servicio = rs.getInt("ID_Servicio");
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"error Buscar Servicio: "+e.getMessage());
        }
        return id_servicio;
    }
    
    
    
    public void Credenciales(){
        
        String Nombre = hs.txt_Titular.getText();
        String Tarjeta = hs.txt_numTarjeta.getText();
        String fecha = hs.txt_fecha.getText();
        int cvv = Integer.parseInt(hs.txt_cvv.getText());
        String tarjetaAux = "";
        String sql = "SELECT NumeroTarjeta FROM tarjetas WHERE NombreTitular = ? AND NumeroTarjeta = ? AND Fecha = ? AND CVV = ? ";
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, Nombre);
            ps.setString(2, Tarjeta);
            ps.setString(3, fecha);
            ps.setInt(4, cvv);
            rs = ps.executeQuery();
            while(rs.next()){
                 tarjetaAux = rs.getString("NumeroTarjeta");
                
            }
            if(tarjetaAux.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Datos Incorrectos.");
                    hs.jf_VerificarPago.setVisible(false);
                    RegistrarServicio("Rechazado");
                    verSaldo();
                }else{
                    ActualizarSaldo();
                    JOptionPane.showMessageDialog(null, "Pago Realizado.");
                    limpiarCredenciales();
                    hs.jf_VerificarPago.setVisible(false);
                    verSaldo();
                    cargarTabla();
                    hs.txt_monto.setText("");
                }
            
        } catch (Exception e) {
            System.out.println("error Credenciales: "+e.getMessage());
        }
    }
    
    
    public void verSaldo(){
        String Tarjeta = hs.lb_numTarjeta.getText();
        
        String sql = "SELECT Saldo FROM tarjetas WHERE  NumeroTarjeta = ?";
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, Tarjeta);
            rs = ps.executeQuery();
            while(rs.next()){
                hs.lb_saldo.setText(""+rs.getFloat("Saldo"));
            }
        }catch(Exception e){
            System.out.println("error en mostrar saldo: "+e.getMessage());
        }
    }
    
    
    
    
    public void cargarTabla() {
        DefaultTableModel modeloTabla = (DefaultTableModel) hs.tb_historial.getModel();
        modeloTabla.setRowCount(0);
        ResultSetMetaData rsmd;
        int columnas;
        int[] ancho = {100, 100, 150,100};
        for (int i = 0; i < modeloTabla.getColumnCount(); i++) {
            hs.tb_historial.getColumnModel().getColumn(i).setPreferredWidth(ancho[i]);
        }
        try {
            String sql = "SELECT ID_Servicio, montoPagar, NumeroTarjeta, Estado FROM historial ORDER BY ID_MovimientoServicio";
                ps = con.prepareStatement(sql);
                rs = ps.executeQuery();
                rsmd = rs.getMetaData();
                columnas = rsmd.getColumnCount();
                while (rs.next()) {
                Object[] fila = new Object[columnas];
                for (int i = 0; i < columnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modeloTabla.addRow(fila);
                
           }
            
            
            
        } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "La transaccion no se concluyo Correctamente\n"
                    + "Error: "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void limpiarCredenciales() {
        hs.txt_Titular.setText("");
        hs.txt_fecha.setText("");
        hs.txt_cvv.setText("");
        hs.txt_numTarjeta.setText("");
      }

}
