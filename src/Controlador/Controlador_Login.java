/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Conexion.Conexion;
import Vista.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author CCNAR
 */
public class Controlador_Login implements ActionListener, KeyListener {

    Login lg;
    Conexion cn = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public Controlador_Login(Login lg) {
        this.lg = lg;
        this.lg.btn_Ingresar.addActionListener(this);
        this.lg.btn_Cancelar.addActionListener(this);
        this.lg.txt_Contra.addKeyListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == lg.btn_Ingresar) {
            Ingresar();
        } else if (e.getSource() == lg.btn_Cancelar) {
            System.exit(0);

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == lg.txt_Contra) {
                Ingresar();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void Ingresar() {
        String usuario = lg.txt_user.getText();
        String contraseña = lg.txt_Contra.getText().toString();
        String sql = "SELECT * FROM usuarios WHERE Usuario = ? AND Contraseña = ?";
        String userAux = "";
        String contraAux = "";
        int id_user = 0;
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            ps.setString(2, contraseña);
            rs = ps.executeQuery();
            while (rs.next()) {
                userAux = (rs.getString("Usuario"));
                contraAux = (rs.getString("Contraseña"));
                id_user = rs.getInt("ID_Usuario");
            }
            if (userAux.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Usuario y/o Contraseña Incorrectos");
                limpiar();
            } else {
                BuscarTarjeta(id_user);
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }

    public void BuscarTarjeta(int id_user) {
        String Nombre = "";
        String Tarjeta = "";
        Float saldo = 0.0f;
        String sql = "SELECT * FROM tarjetas WHERE ID_Usuario = ?";
        try {
            con = cn.getConexion();
            ps = con.prepareStatement(sql);
            ps.setInt(1, id_user);
            rs = ps.executeQuery();
            while (rs.next()) {
                Nombre = rs.getString("NombreTitular");
                Tarjeta = rs.getString("NumeroTarjeta");
                saldo = rs.getFloat("Saldo");
            }
            if(Tarjeta.isEmpty()){
                JOptionPane.showMessageDialog(null, "Su Tarjeta no ha sido Activada, Verifique con el Administrador");
                limpiar();
            }else{
                Historial sv = new Historial();
                sv.lb_datos_cliente.setText(Nombre);
                sv.lb_numTarjeta.setText("" + Tarjeta);
                sv.lb_saldo.setText("" + saldo);
                limpiar();
                sv.setVisible(true);
                lg.setVisible(false);
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void limpiar() {
        lg.txt_user.setText("");
        lg.txt_Contra.setText("");
        lg.txt_user.requestFocus(true);
    }

}
